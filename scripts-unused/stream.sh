#!/bin/bash

VBR="2500k"
FPS="30"
QUAL="medium"
YOUTUBE_URL="rtmp://blablabla/live2"
SOURCE="udp;//239.255.139.0:1234"
KEY=""

ffmpeg \
	-i "$1" -deinterlace\
	-vcodec libx264 -pix_fmt yuv420p -preset $QUAL -r $FPS -g $(($FPS * 2)) -b:v $VBR \
	-acodec libmp3lame -ar 44100 -threads 4 -qscale 3 -b:a 712000 -bufsize 512k \
	-f flv "$YOUTUBE_URL/$KEY"
