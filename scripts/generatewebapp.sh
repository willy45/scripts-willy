echo "Enter URL: "
read URL
echo "Enter Application Name: "
read APPNAME
echo "Enter Platform (linux/osx/mac/windows): "
read PLATFORM
echo "Enter CPU Architecture(x64/arm64): "
read ARCH

if ! command -v nativefier &> /dev/null
then
	echo "Nativefier could not be found. Please install it!"
	exit
else
	start_time=$(date +%s.%3N)
	nativefier "$URL" --name "$APPNAME" --platform $PLATFORM --arch $ARCH
	end_time=$(date +%s.%3N)
	elapsed=$(echo "scale=3; $end_time - $start_time" | bc)
	echo -e "\nElapsed time: $elapsed milliseconds."
	echo -e "\nYour Webapp Directory is: $APPNAME-$PLATFORM-$ARCH"
	echo -e "\nThe executable is located in the Webapp directory, where the file name is $APPNAME"
fi
