shutdown -P 23:00

timee=$(date +%H:%M)
if [[ "$timee" > "22:25" ]] && [[ "$timee" < "22:32" ]];
then
	notify-send -u critical "System" "System is powering off on 23:00 sharp!"
fi

if [[ "$timee" > "22:41" ]] && [[ "$timee" < "22:48" ]];
then
	notify-send -u critical "System" "Save all your work! System is powering off in around 15 minutes!"
fi
