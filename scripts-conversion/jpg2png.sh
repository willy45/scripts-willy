#!/bin/bash
echo "Please enter directory path (eg. /home/user/jpg-files/)"
read dir
for image in `find $dir -type f`; do
    convert "$image" "${image%.jpg}.png"
    echo "Image $image converted to ${image%.jpg}.png"
done