import subprocess
import os

input_directory = r"/root/vid/"
output_directory = r"/root/out/"
directory_list = os.listdir(input_directory)

for file in directory_list:
	full_path = os.path.join(input_directory,file)
	output_path = os.path.join(output_directory,file.replace(".mp4",".mkv"))
	print("Converting {} to .mkv".format(full_path))
	cmd = [r"/usr/bin/HandBrakeCLI","-i",f"{full_path}","-o",f"{output_path}","-e","x264","-q","31","-B","160","-v","-f","av_mkv","--vfr"]
	process = subprocess.Popen(cmd,stdout=subprocess.PIPE,stderr=subprocess.STDOUT,universal_newlines=True)
	for line in process.stdout:
		print(line)
	#print("Finised")

print("Done")

