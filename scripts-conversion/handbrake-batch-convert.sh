for file in $( ls | grep -iF ".mp4")
do
	echo "Converting file: $file"
	HandBrakeCLI -i $file -o out/$file.mkv -e x264 -q 31 -B 160 -v -f av_mkv --vfr
done

