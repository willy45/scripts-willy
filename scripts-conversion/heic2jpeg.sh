echo "Enter image directory: "
read IMAGEDIR
echo "Output image directory: "
read OUTPUTDIR

if ! command -v heif-convert &> /dev/null
then
    echo "heif-convert COMMAND could not be found Please install 'libheif-examples' or 'libheif' first."
    exit
else
    fileExtension="jpg"
    while getopts :p flag; do
        case ${flag} in
            # -p flag: convert heic files to png format instead
            p) fileExtension="png"
            ;;
        esac
    done

    start_time=$(date +%s.%3N)

    for file in $( ls $IMAGEDIR | grep -iF ".heic")
    do
        echo "Converting file: $file"
        currFileExtension=`echo $file | grep -iFo "heic"`
        sedCommand="s/${currFileExtension}/${fileExtension}/g;s/HEIC/${fileExtension}/g"
        outputFileName=`echo $file | sed -e $sedCommand`
        heif-convert $IMAGEDIR/$file $OUTPUTDIR/$outputFileName
    done

    end_time=$(date +%s.%3N)
    elapsed=$(echo "scale=3; $end_time - $start_time" | bc)
    echo -e "\nElapsed time: \e[32m$elapsed \e[39mmilliseconds."
fi