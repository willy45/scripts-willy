#!/bin/bash
echo "Please enter directory path (eg. /home/user/png-files/)"
read dir
for image in `find $dir -type f`; do
    convert "$image" "${image%.png}.jpg"
    echo "Image $image converted to ${image%.png}.jpg"
done