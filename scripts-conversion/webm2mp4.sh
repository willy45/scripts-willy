#!/bin/bash
echo "Please enter directory path (eg. /home/user/webm-files/)"
read dir
for i in `find $dir -type f`; do
  ffmpeg -i $i -c:v copy $i.mp4
done